import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';
import { PasoOneComponent } from './components/steps/paso-one/paso-one.component';
import { PasoTwoComponent } from './components/steps/paso-two/paso-two.component';
import { PasoThreeComponent } from './components/steps/paso-three/paso-three.component';
import { AppComponent } from './app.component';



const routes: Routes = [

    {
        path: "pasouno",
        component: PasoOneComponent
    },

    {
        path: "pasodos",
        component: PasoTwoComponent
    },
    {
        path: "pasotres",
        component: PasoThreeComponent
    }

]
@NgModule({
    imports: [RouterModule.forRoot(routes),RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule],
    providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}]

  })
export class AppRoutingModule { }
