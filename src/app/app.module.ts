import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FacturasService } from './services/facturas.service';

import { AppComponent } from './app.component';
import { PasoOneComponent } from './components/steps/paso-one/paso-one.component';
import { PasoTwoComponent } from './components/steps/paso-two/paso-two.component';
import { PasoThreeComponent } from './components/steps/paso-three/paso-three.component';
import { AppRoutingModule } from './app.routing.module';

@NgModule({
  declarations: [
    AppComponent,
    PasoOneComponent,
    PasoTwoComponent,
    PasoThreeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FacturasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
