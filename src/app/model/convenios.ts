export class Convenios {
   public numeroConvenio:number;
   public codigoCategoria:number;
   public nombreConvenio:string;
   public pagoSucursalVirtual
   public pagoCaja:string;
   public pagoCajero:string;
   public pagoCorresponsalBancario:string;
}

