import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {

	url: any;
	urlConvenios: any;

	constructor(protected http: HttpClient) { }

	getUrl() {
	    if (!this.url) {
	        this.url = environment.url;
	        //this.url = environment.url_local;
	    }
	    return this.url;
	}
	getUrlConvenios() {
	    if (!this.urlConvenios) {
	        this.urlConvenios = environment.urlConvenios;
	        //this.url = environment.url_local;
	    }
	    return this.urlConvenios;
	}

	getCategorias() {
		return this.http.get(this.getUrl());
	}

	getCategoriaId(codicoCategoria: number) {
		return this.http.get(this.getUrlConvenios()+"/"+ codicoCategoria);
	}
}
