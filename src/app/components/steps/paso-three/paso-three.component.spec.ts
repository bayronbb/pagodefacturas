import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasoThreeComponent } from './paso-three.component';

describe('PasoThreeComponent', () => {
  let component: PasoThreeComponent;
  let fixture: ComponentFixture<PasoThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasoThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasoThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
