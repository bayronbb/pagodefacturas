import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Convenios } from 'src/app/model/convenios';
import { Categories } from 'src/app/model/categories';
import { Router } from "@angular/router";

@Component({
  selector: 'app-paso-three',
  templateUrl: './paso-three.component.html',
  styleUrls: ['./paso-three.component.scss']
})
export class PasoThreeComponent implements OnInit {

  @Input() showComponent: boolean;

  public convenio : Convenios;
  public categoria : Categories;
  public isPagoCaja : boolean;
  public isPagoCajero :boolean;
  public isPagoCorresponsalBancario :boolean;
  public isPagoSucursalVirtual :boolean;
  constructor(private router:Router, private el: ElementRef) { }

  ngOnInit(): void {
    this.convenio= JSON.parse(localStorage.getItem('convenioSeleccionado'));
    this.categoria= JSON.parse(localStorage.getItem('categariaSeleccionada'));
    this.isPagoSucursalVirtual = this.convenio.pagoSucursalVirtual ==='Si'? true:false;
    this.isPagoCajero = this.convenio.pagoCajero ==='Si'? true:false;
    this.isPagoCorresponsalBancario = this.convenio.pagoCorresponsalBancario ==='Si'? true:false;
    this.isPagoCaja = this.convenio.pagoCaja ==='Si'? true:false;

		this.scrollToPagoFacturasPasoTresInit();

  }

	getRoute2(){
	    if (this.router.url === '/pasotres'){
	      return "text-active";
	   	}
  }
  
  private scrollToPagoFacturasPasoTresInit() {
		const pagoFacturasPasoTres: HTMLElement = this.el.nativeElement.querySelector(
		  "section"
		);
		window.scroll({
		  top: this.getTopOffset(pagoFacturasPasoTres),
		  left: 0,
		  behavior: "smooth"
		});
	  }
	
	  private getTopOffset(controlEl: HTMLElement): number {
		const labelOffset = 50;
		return controlEl.getBoundingClientRect().top + window.scrollY - labelOffset;
	  }
}
