import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FacturasService } from '../../../services/facturas.service';
import { Convenios } from 'src/app/model/convenios';
import { Categories } from 'src/app/model/categories';
import { Router } from "@angular/router";

@Component({
  selector: 'app-paso-two',
  templateUrl: './paso-two.component.html',
  styleUrls: ['./paso-two.component.scss']
})
export class PasoTwoComponent implements OnInit {
	public convenios: Convenios[];
	public categoria : Categories;

	show = 10;

	constructor(protected facturasService: FacturasService,  private router:Router , private el: ElementRef) { }

	ngOnInit(): void {
		this.convenios =[];
		this.categoria = JSON.parse(localStorage.getItem('categariaSeleccionada'));
		this.facturasService.getCategoriaId(this.categoria.codigoCategoria).subscribe((convenios:Convenios[]) => { // Success
			this.convenios = convenios;
		},
	      (error) => {
	        console.error(error);
	      }
		);
		this.scrollToPagoFacturasPasoTwoInit();

	}
	getRoute1(){
	    if (this.router.url === '/pasodos'){
	      return "text-active";
	   	}
	}

	verMas(){
		this.show += 10;
	}

	goToStepThree(convenio:Convenios){
		localStorage.setItem('convenioSeleccionado', JSON.stringify(convenio));

	}
	private scrollToPagoFacturasPasoTwoInit() {
		const pagoFacturasPasoTwo: HTMLElement = this.el.nativeElement.querySelector(
		  "section"
		);
		window.scroll({
		  top: this.getTopOffset(pagoFacturasPasoTwo),
		  left: 0,
		  behavior: "smooth"
		});
	  }
	
	  private getTopOffset(controlEl: HTMLElement): number {
		const labelOffset = 50;
		return controlEl.getBoundingClientRect().top + window.scrollY - labelOffset;
	  }


	

}
