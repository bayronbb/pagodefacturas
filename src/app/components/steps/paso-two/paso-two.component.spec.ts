import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasoTwoComponent } from './paso-two.component';

describe('PasoTwoComponent', () => {
  let component: PasoTwoComponent;
  let fixture: ComponentFixture<PasoTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasoTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasoTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
