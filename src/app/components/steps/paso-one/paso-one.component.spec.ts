import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasoOneComponent } from './paso-one.component';

describe('PasoOneComponent', () => {
  let component: PasoOneComponent;
  let fixture: ComponentFixture<PasoOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasoOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasoOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
