import { Component, OnInit, ElementRef, Inject, Output, EventEmitter } from '@angular/core';
import { Categories } from '../../../model/categories';
import { FacturasService } from '../../../services/facturas.service';
import { Router } from "@angular/router";
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-paso-one',
  templateUrl: './paso-one.component.html',
  styleUrls: ['./paso-one.component.scss']
})
export class PasoOneComponent implements OnInit {
	  public categorias: Categories [] = [];
	  public categoriaSeleccionada: Categories;
	  public showComponent : boolean; 


  	constructor( protected facturasService: FacturasService,  private el: ElementRef,private router:Router) {

		
	  }
	  

  	ngOnInit(): void {

	    this.showComponent= true;
  		this.facturasService.getCategorias().subscribe((categories:Categories[]) => { // Success
			 this.categorias = categories;
		  },
	      (error) => {
	        console.error(error);
	      }
		);
		this.scrollToPagoFacturasPasoUnoInit();

	  }

	getRoute(){
	    if (this.router.url === '/pasouno'){
	      return "text-active";
	   	}
	}
	getRoute1(){
	    if (this.router.url === '/pasodos'){
	      return "text-active";
	   	}
	}
	getRoute2(){
	    if (this.router.url === '/pasotres'){
	      return "text-active";
	   	}
	}

	status: boolean = false;
	  selectCategory(categoria:Categories, event){

		this.categoriaSeleccionada = categoria;	 
		const dom: HTMLElement = this.el.nativeElement;
		const allTooltips = dom.querySelectorAll('.text-active');

		allTooltips.forEach(items => {

		items.classList.remove('text-active');
		});
	        

		if(!event.target.classList.contains('text-active')){
			let x = event.target.tagName;
			if( x == 'IMG'){
				event.target.parentElement.classList.add('text-active');
			}else{

				event.target.classList.add('text-active');
			}

		}
	  }
	  
	  goToStepTwo(){
		  if (this.categoriaSeleccionada){
			localStorage.setItem('categariaSeleccionada', JSON.stringify(this.categoriaSeleccionada))
			this.showComponent= false;
		 }

	  }
	  private scrollToPagoFacturasPasoUnoInit() {
		const pagoFacturasPasoUno: HTMLElement = this.el.nativeElement.querySelector(
		  "section"
		);
		window.scroll({
		  top: this.getTopOffset(pagoFacturasPasoUno),
		  left: 0,
		  behavior: "smooth"
		});
	  }
	
	  private getTopOffset(controlEl: HTMLElement): number {
		const labelOffset = 50;
		return controlEl.getBoundingClientRect().top + window.scrollY - labelOffset;
	  }

}
