export const environment = {
  production: true,
  	url: "https://conveniospagofacturas.isobarapi.com/api/v1/rest/convenios/pago/facturas/categorias",
	urlConvenios: "https://conveniospagofacturas.isobarapi.com/api/v1/rest/convenios/pago/facturas"
};
